/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author roman
 */
public class SelectDB {
  
    public static void main(String[] args) {
        Connection conn = null;
        String url = "jdbc:sqlite:C:\\Users\\roman\\Documents\\NetBeansProjects\\JDBC/dcoffee.db";
        try {

            

            conn = DriverManager.getConnection(url);

            System.out.println("Connection to SQLite has been established.");

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return;
        }

        String sql = "SELECT * FROM category";
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            while (rs.next()) {
                System.out.println(rs.getInt("category_id") + " " 
                        + rs.getString("category_name"));
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }

        //close
        if (conn != null) {
            try {
                conn.close();

            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
            }
        }
    }
}

