/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 *
 * @author roman
 */
public class InsertDB {
   public static void main(String[] args) {
        Connection conn = null;
        try {

            String url = "jdbc:sqlite:C:\\Users\\roman\\Documents\\NetBeansProjects\\JDBC/dcoffee.db";

            conn = DriverManager.getConnection(url);

            System.out.println("Connection to SQLite has been established.");

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return;
        }
//insert
        String sql = "INSERT INTO category(category_id,category_name) VALUES ( ?,?)";
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, 3);
            stmt.setString(2, "candy");
            int status  = stmt.executeUpdate();
           // ResultSet key = stmt.getGeneratedKeys();
         //   key.next();
          //  System.out.println(" "+ key.getInt(1));
           
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }

        //close
        if (conn != null) {
            try {
                conn.close();

            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
            }
        }
    }
}

